<?php

namespace App\Orchid\Screens;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Action;
use Orchid\Screen\Fields\Picture;
use Orchid\Support\Facades\Alert;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;

class CategoryEditScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */

    public $name = 'Create category';

    public $exists = false;

    public function query(Category $category): iterable
    {
        $this->exists = $category->exists;
        if ($this->exists) {
            $this->name = 'Edit category';
        }
        return [
            'category' => $category
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'CategoryEditScreen';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Create category')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),
            Button::make('Update category')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),
            Button::make('Delete category')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists)
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::rows([
                Input::make('name')
                    ->title('Name')
                    ->placeholder('Type name')
                    ->help('Help message'),
                Picture::make('picture')
                    ->targetRelativeUrl()
                    ->title('Picture')
            ])
        ];
    }

    /**
     * @param Category $category
     * @param CategoryRequest $request
     * @return RedirectResponse
     */
    public function createOrUpdate(Category $category, CategoryRequest $request): RedirectResponse
    {
        $category->fill($request->all())->save();
        Alert::info('You have successfully create a new category');
        return redirect()->route('platform.categories.list');
    }

    /**
     * @param Category $category
     * @return RedirectResponse
     */
    public function remove(Category $category): RedirectResponse
    {
        $category->delete() ? Alert::info('Category successfully deleted.') : Alert::error('Oops!');
        return redirect()->route('platform.categories.list');
    }

}
