<?php

namespace App\Orchid\Screens;

use App\Models\Request;
use App\Orchid\Layouts\RequestListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class RequestListScreen extends Screen
{

    public $name = 'Заявки';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'requests' => Request::paginate()
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Категории';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Создать новую')
                ->icon('icon-plus')
                ->route('platform.requests.edit'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            RequestListLayout::class
        ];
    }
}
