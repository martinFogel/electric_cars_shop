<?php

namespace App\Orchid\Screens;

use App\Http\Requests\CarRequest;
use App\Models\Brand;
use App\Models\Car;
use App\Models\Category;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class CarEditScreen extends Screen
{

    public $name = 'Create car';

    public $exists = false;

    /**
     * Query data.
     *
     * @param Car $car
     * @return array
     */
    public function query(Car $car): iterable
    {
        $this->exists = $car->exists;
        if ($this->exists) {
            $this->name = 'Edit car';
        }
        return [
            'category' => $car
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Редактировать транспорт';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Create car')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),
            Button::make('Update car')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),
            Button::make('Delete car')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists)
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::rows([
                Input::make('name')
                    ->title('Name')
                    ->placeholder('Type name')
                    ->help('Help message'),
                Picture::make('picture')
                    ->targetRelativeUrl()
                    ->title('Picture'),
                TextArea::make('description')
                    ->title('Main text'),
                Relation::make('brand_id')
                    ->title('Brand')
                    ->fromModel(Brand::class, 'name', 'id'),
                Relation::make('category_id')
                    ->title('Category')
                    ->fromModel(Category::class, 'name', 'id'),
            ])
        ];
    }

    /**
     * @param Car $car
     * @param CarRequest $request
     * @return RedirectResponse
     */
    public function createOrUpdate(Car $car, CarRequest $request): RedirectResponse
    {
        $car->fill($request->all())->save();
        Alert::info('You have successfully create a new car');
        return redirect()->route('platform.cars.list');
    }

    /**
     * @param Car $car
     * @return RedirectResponse
     */
    public function remove(Car $car): RedirectResponse
    {
        $car->delete() ? Alert::info('Car successfully deleted.') : Alert::error('Oops!');
        return redirect()->route('platform.cars.list');
    }
}
