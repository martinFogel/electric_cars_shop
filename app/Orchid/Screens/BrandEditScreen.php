<?php

namespace App\Orchid\Screens;

use App\Http\Requests\BrandRequest;
use App\Models\Brand;
use Orchid\Screen\Action;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\TextArea;
use Orchid\Support\Facades\Alert;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;

class BrandEditScreen extends Screen
{

    public $name = 'Create brand';
    public $exists = false;

    /**
     * Query data.
     *
     * @param Brand $brand
     * @return array
     */
    public function query(Brand $brand): iterable
    {
        $this->exists = $brand->exists;
        if ($this->exists) {
            $this->name = 'Edit brand';
        }
        return [
            'brand' => $brand
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Редактировать бренд';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Create brand')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),
            Button::make('Update brand')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),
            Button::make('Delete brand')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists)
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::rows([
                Input::make('name')
                    ->title('Name')
                    ->placeholder('Type name')
                    ->help('Help message'),
                Picture::make('picture')
                    ->targetRelativeUrl()
                    ->title('Picture'),
                TextArea::make('description')
                    ->title('Main text'),
            ])
        ];
    }

    /**
     * @param Brand $brand
     * @param BrandRequest $request
     * @return RedirectResponse
     */
    public function createOrUpdate(Brand $brand, BrandRequest $request): RedirectResponse
    {
        $brand->fill($request->all())->save();
        Alert::info('You have successfully create a new brand');
        return redirect()->route('platform.brands.list');
    }

    /**
     * @param Brand $brand
     * @return RedirectResponse
     */
    public function remove(Brand $brand): RedirectResponse
    {
        $brand->delete() ? Alert::info('Brand successfully deleted.') : Alert::error('Oops!');
        return redirect()->route('platform.brands.list');
    }
}
