<?php

namespace App\Orchid\Screens;

use App\Models\Brand;
use App\Orchid\Layouts\BrandListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class BrandListScreen extends Screen
{

    public $name = 'Бренды';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'brands' => Brand::paginate()
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Бренды';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Создать новую')
                ->icon('icon-plus')
                ->route('platform.brands.edit'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            BrandListLayout::class
        ];
    }
}
