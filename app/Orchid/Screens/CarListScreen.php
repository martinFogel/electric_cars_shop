<?php

namespace App\Orchid\Screens;

use App\Models\Car;
use App\Orchid\Layouts\CarListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class CarListScreen extends Screen
{

    public $name = 'Транспорт';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'cars' => Car::paginate()
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Транспорт';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Создать новую')
                ->icon('icon-plus')
                ->route('platform.cars.edit'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            CarListLayout::class
        ];
    }
}
