<?php

namespace App\Orchid\Screens;

use App\Http\Requests\RequestRequest;
use App\Models\Car;
use App\Models\Request;
use Illuminate\Http\RedirectResponse;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class RequestEditScreen extends Screen
{


    public $name = 'Create request';

    public $exists = false;

    /**
     * Query data.
     *
     * @param Request $request
     * @return array
     */
    public function query(Request $request): iterable
    {
        $this->exists = $request->exists;
        if ($this->exists) {
            $this->name = 'Edit request';
        }
        return [
            'request' => $request
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'RequestEditScreen';
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Create request')
                ->icon('icon-pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),
            Button::make('Update request')
                ->icon('icon-note')
                ->method('createOrUpdate')
                ->canSee($this->exists),
            Button::make('Delete request')
                ->icon('icon-trash')
                ->method('remove')
                ->canSee($this->exists)
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::rows([
                Input::make('name')
                    ->title('Name')
                    ->placeholder('Type name')
                    ->help('Help message'),
                Input::make('phone')
                    ->title('Phone')
                    ->placeholder('Type phone')
                    ->help('Help message'),
                Relation::make('car_id')
                    ->title('Car')
                    ->fromModel(Car::class, 'name', 'id'),
            ])
        ];
    }

    /**
     * @param Request $request1
     * @param RequestRequest $request
     * @return RedirectResponse
     */
    public function createOrUpdate(Request $request1, RequestRequest $request): RedirectResponse
    {
        $request1->fill($request->all())->save();
        Alert::info('You have successfully create a new request');
        return redirect()->route('platform.requests.list');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function remove(Request $request): RedirectResponse
    {
        $request->delete() ? Alert::info('Request successfully deleted.') : Alert::error('Oops!');
        return redirect()->route('platform.requests.list');
    }
}
