<?php

namespace App\Orchid\Layouts;

use App\Models\Request;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class RequestListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'requests';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('name', 'Имя')
                ->render(function (Request $request) {
                    return Link::make($request->name)
                        ->route('platform.requests.edit', $request);
                }),
            TD::make('phone', 'Телефон')
                ->render(function (Request $request) {
                    return Link::make($request->phone)
                        ->route('platform.requests.edit', $request);
                }),
        ];
    }
}
