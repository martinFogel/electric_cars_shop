<?php

namespace App\Orchid\Layouts;

use App\Models\Brand;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use Orchid\Screen\Actions\Link;

class BrandListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'brands';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('name', 'Имя')
                ->render(function (Brand $brand) {
                    return Link::make($brand->name)
                        ->route('platform.brands.edit', $brand);
                }),

            TD::make('picture', 'Картинка')
                ->render(function (Brand $brand) {
                    $picture = asset($brand->picture);
                    return "<img style='width: 100px; height: 100px' src='{$picture}' alt='picture'/>";
                }),

            TD::make('description', 'Описание')
                ->render(function (Brand $brand) {
                    return Link::make($brand->description)
                        ->route('platform.brands.edit', $brand);
                }),
        ];
    }
}
