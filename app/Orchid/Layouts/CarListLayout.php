<?php

namespace App\Orchid\Layouts;

use App\Models\Car;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CarListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'cars';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('name', 'Имя')
                ->render(function (Car $car) {
                    return Link::make($car->name)
                        ->route('platform.cars.edit', $car);
                }),

            TD::make('picture', 'Картинка')
                ->render(function (Car $car) {
                    $picture = asset($car->picture);
                    return "<img style='width: 100px; height: 100px' src='{$picture}' alt='picture'/>";
                }),
            TD::make('description', 'Описание')
                ->render(function (Car $car) {
                    return Link::make($car->description)
                        ->route('platform.cars.edit', $car);
                }),
        ];
    }
}
