<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestRequest;
use App\Models\Request;

class RequestController extends Controller
{
    public function store(RequestRequest $request)
    {
        $request = new Request($request->all());
        $request->save();
        return redirect()->route('cars.index')->with('success', 'Заявка была успешно отправлена!');
    }
}
