<?php

namespace App\Http\Controllers;

use App\Models\Car;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class CarController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $cars = Car::all();
        return view('client.cars.index', compact('cars'));
    }

    /**
     * @param Car $car
     * @return Application|Factory|View
     */
    public function show(Car $car)
    {
       return view('client.cars.show', compact('car'));
    }
}
