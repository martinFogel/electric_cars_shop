<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CarRequest;
use App\Models\Brand;
use App\Models\Car;
use App\Models\Category;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $cars = Car::all();
        return view('admin.cars.index', compact('cars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $brands = Brand::all();
        $categories = Category::all();
        return view('admin.cars.create', compact('brands', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CarRequest $request
     * @return RedirectResponse
     */
    public function store(CarRequest $request)
    {
        $data = $request->all();
        $file = $request->file('picture');
        if (!is_null($file)) {
            $path = $file->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $car = new Car($data);
        $car->save();
        return redirect()->route('admin.cars.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Car $car
     * @return Application|Factory|View
     */
    public function show(Car $car)
    {
        return view('admin.cars.show', compact('car'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Car $car
     * @return Application|Factory|View
     */
    public function edit(Car $car)
    {
        $categories = Category::all();
        $brands = Brand::all();
        return view('admin.cars.edit', compact('categories', 'brands', 'car'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CarRequest $request
     * @param Car $car
     * @return RedirectResponse
     */
    public function update(CarRequest $request, Car $car)
    {
        $data = $request->all();
        if($request->hasFile('picture')){
            $file = $request->file('picture');
            $path = $file->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $car->update($data);
        return redirect()->action([self::class, 'index']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Car $car
     * @return RedirectResponse
     */
    public function destroy(Car $car)
    {
        $car->delete();
        return redirect()->action([self::class, 'index']);
    }
}
