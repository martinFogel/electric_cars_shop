<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $brands = Brand::all();
        return view('client.brands.index', compact('brands'));
    }

    /**
     * @param Brand $brand
     * @return Application|Factory|View
     */
    public function show(Brand $brand)
    {
        return view('client.brands.show', compact('brand'));
    }
}
