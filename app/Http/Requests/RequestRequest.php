<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:150'],
            'phone' => ['required', 'regex:/([^(<)(>)])$/u'],
            'car_id' => ['required']
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Нужно ввести данные в поле: :attribute',
            'name.max' => 'Слишком много символов в поле: :attribute',
            'phone.required' => 'Нужно ввести данные в поле: :attribute',
            'phone.regex' => 'Телефон не соответствует формату: :attribute',
        ];
    }
}
