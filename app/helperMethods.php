<?php

function vehicles()
{
    return \App\Models\Car::all();
}

function categories()
{
    return \App\Models\Category::all();
}
