@extends('layouts.admin')

@section('content')
    <p>
        picture:  <img src="{{asset('/storage/' . $brand->picture)}}" alt="{{asset('/storage/' . $brand->picture)}}"
                       style="width: 300px; height: 300px">
    </p>
    <p>
        name: {{$brand->name}}
    </p>
@endsection
