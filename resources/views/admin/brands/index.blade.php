@extends('layouts.admin')

@section('content')

    <a href="{{route('admin.brands.create')}}" class="btn btn-primary">Создать новый бренд</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Имя</th>
            <th scope="col">Картинка</th>
            <th scope="col">Описание</th>
            <th scope="col">Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach($brands as $brand)
            <tr>
                <td>{{$brand->name}}</td>
                @if($brand->picture)
                    <td>
                        <img src="{{asset('/storage/' . $brand->picture)}}"
                             alt="{{asset('/storage/' . $brand->picture)}}"
                             style="width: 150px; height: 150px">
                    </td>
                @endif
                <td>{{$brand->description}}</td>
                <td>
                    <div class="btn-group">
                        <form action="{{route('admin.brands.destroy', $brand)}}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-outline-danger">Удалить</button>
                        </form>
                        <a href="{{route('admin.brands.edit', $brand)}}" class="btn btn-outline-dark">Редактировать</a>
                        <a href="{{route('admin.brands.show', $brand)}}" class="btn btn-outline-info">Показать</a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection()
