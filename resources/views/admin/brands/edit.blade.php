@extends('layouts.admin')

@section('content')

    <form action="{{route('admin.brands.update', $brand)}}" enctype="multipart/form-data" method="post">
        @csrf
        @method('put')
        <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" class="form-control" name="name" id="name" value="{{$brand->name}}">
        </div>
        <div class="mb-3">
            <label for="description">
                <textarea name="description" id="description" cols="30" rows="10">{{$brand->description}}</textarea>
            </label>
        </div>
        <img style="height: 100px; width: 100px" src="{{asset('/storage/' . $brand->picture)}}" alt="{{asset('/storage/' . $brand->picture)}}">
        <div class="mb-3">
            <label for="picture" class="form-label">Picture</label>
            <input type="file" class="form-control" name="picture" id="picture">
        </div>
        <button type="submit" class="btn btn-info">Update</button>
    </form>
@endsection
