@extends('layouts.admin')

@section('content')
    <form action="{{route('admin.cars.store', $car)}}" enctype="multipart/form-data" method="post">
        @csrf
        @method('put')
        <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" class="form-control" name="name" id="name" value="{{$car->name}}">
        </div>
        <div class="mb-3">
            <label for="description">
                <textarea name="description" id="description" cols="30" rows="10">{{$car->description}}</textarea>
            </label>
        </div>
        <div class="mb-3">
            <label for="brand_id">Choose brand</label>
            <select class="custom-select" name="brand_id">
                @foreach($brands as $brand)
                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-3">
            <label for="category_id">Choose category</label>
            <select class="custom-select" name="category_id">
                @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        </div>
        @if($car->picture)
                <img src="{{asset('/storage/' . $car->picture)}}"
                     alt="{{asset('/storage/' . $car->picture)}}"
                     style="width: 150px; height: 150px">
        @endif
        <div class="mb-3">
            <label for="picture" class="form-label">Picture</label>
            <input type="file" class="form-control" name="picture" id="picture">
        </div>
        <button type="submit" class="btn btn-info">Create</button>
    </form>
@endsection
