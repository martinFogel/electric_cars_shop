@extends('layouts.admin')

@section('content')
    <br>
    <br>
    <p>
        picture:    <img src="{{asset('/storage/' . $car->picture)}}" alt="{{asset('/storage/' . $car->picture)}}"
                         style="width: 300px; height: 300px">
    </p>
    <p>
        name:    {{$car->name}}
    </p>
    <p>
        description: {{$car->description}}
    </p>
    <p>
        brand: {{$car->brand->name}}
    </p>
    <p>
        category: {{$car->category->name}}
    </p>
    <a href="{{route('admin.categories.index')}}">Back</a>
@endsection
