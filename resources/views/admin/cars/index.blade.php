@extends('layouts.admin')

@section('content')

    <a href="{{route('admin.cars.create')}}" class="btn btn-primary">Create a new car</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Picture</th>
            <th scope="col">Category</th>
            <th scope="col">Brand</th>
            <th scope="col">Description</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($cars as $car)
        <tr>
            <td>{{$car->name}}</td>
            @if($car->picture)
                <td>
                    <img src="{{asset('/storage/' . $car->picture)}}"
                         alt="{{asset('/storage/' . $car->picture)}}"
                         style="width: 150px; height: 150px">
                </td>
            @endif
            @if(!empty($car->category->name))
            <td>{{$car->category->name}}</td>
            @else
                <td>---</td>
                @endif
            @if(!empty($car->brand->name))
                <td>{{$car->brand->name}}</td>
            @else
                <td>---</td>
            @endif
            <td>{{$car->description}}</td>
            <td>
                <div class="btn-group">
                    <form action="{{route('admin.cars.destroy', $car)}}" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-outline-danger">Delete</button>
                    </form>
                    <a href="{{route('admin.cars.edit', $car)}}" class="btn btn-outline-dark">Edit</a>
                    <a href="{{route('admin.cars.show', $car)}}" class="btn btn-outline-info">Show</a>
                </div>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

@endsection()
