@extends('layouts.admin')

@section('content')
    <form action="{{route('admin.categories.update', $category)}}" enctype="multipart/form-data" method="post">
        @csrf
        @method('put')
        <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" class="form-control" name="name" id="name" value="{{$category->name}}">
        </div>
        <img style="height: 100px; width: 100px" src="{{asset('/storage/' . $category->picture)}}" alt="{{asset('/storage/' . $category->picture)}}">
        <div class="mb-3">
            <label for="picture" class="form-label">Picture</label>
            <input type="file" class="form-control" name="picture" id="picture">
        </div>
        <button type="submit" class="btn btn-warning">Edit</button>
    </form>
@endsection

