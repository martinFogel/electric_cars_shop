@extends('layouts.admin')

@section('content')
    <br>
    <br>
    <p>
    picture:    <img src="{{asset('/storage/' . $category->picture)}}" alt="{{asset('/storage/' . $category->picture)}}"
             style="width: 300px; height: 300px">
    </p>
    <p>
    name:    {{$category->name}}
    </p>

    <a href="{{route('admin.categories.index')}}">Back</a>
@endsection
