@extends('layouts.admin')

@section('content')

    <a href="{{route('admin.categories.create')}}" class="btn btn-primary">Create a new category</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Picture</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr>
                <td>{{$category->name}}</td>
                @if($category->picture)
                    <td>
                        <img src="{{asset('/storage/' . $category->picture)}}"
                             alt="{{asset('/storage/' . $category->picture)}}"
                             style="width: 150px; height: 150px">
                    </td>
                @endif
                <td>
                    <div class="btn-group">
                        <form action="{{route('admin.categories.destroy', $category)}}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-outline-danger">Delete</button>
                        </form>
                        <a href="{{route('admin.categories.edit', $category)}}" class="btn btn-outline-dark">Edit</a>
                        <a href="{{route('admin.categories.show', $category)}}" class="btn btn-outline-info">Show</a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection()
