@extends('layouts.user')

@section('content')

    <div class="container">
        <div class="border-bottom pb-3 mb-3">
            <h3 class="pt-5 border-bottom pb-1 mb-3 ">Название модели: {{$car->name}}</h3>
            <div class="row row-cols-1 row-cols-md-2 row-cols-lg-2 .img-fluid. w-100%  h-auto ">
                <div class="col mb-4">
                    @if(!is_null($car->category))
                        <h4>Категория: {{$car->category->name}}</h4>
                    @endif
                    <br>
                    @if(!is_null($car->brand))
                        <h4>Бренд: {{$car->brand->name}}</h4>
                    @endif
                    <br>
                    <h4>Информация о модели:</h4>
                    @if (!is_null($car->description))
                        <p>{{$car->description}}</p>
                    @else
                        Нет описания
                    @endif
                </div>
                <div class="col mb-1 ">
                    <img src="{{asset('/storage/' . $car->picture)}}"
                         class="rounded mx-auto d-block img-fluid rounded float-right"
                         alt="{{asset('/storage/' . $car->picture)}}" width="400px" height="400px">
                </div>
            </div>
        </div>
        <a href="/" style="color: orange; font-size: large">На главную страницу</a>
        <br>
        <br>
        <br>
        <h2>Оставить заявку на данную модель</h2>
        <form action="{{route('requests.store')}}" method="post">
            @csrf
            <input type="hidden" value="{{$car->id}}" name="car_id">
            <div class="mb-3">
                <label for="name" class="form-label">Имя</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="mb-3">
                <label for="phone" class="form-label">Телефон</label>
                <input type="text" class="form-control" id="phone" name="phone">
            </div>
            <button type="submit" class="btn btn-warning">Отправить</button>
        </form>
        <br>
        <br>
    </div>
@endsection
