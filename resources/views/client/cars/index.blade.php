@extends('layouts.user')

@section('content')

    @if(session('success'))
        <div class="alert alert-primary" role="alert">
            {{session('success')}}
        </div>
    @endif

    <!-- ======= Hero Section ======= -->
    <section id="hero">
        <div class="hero-container" data-aos="fade-up">
            <h1>Начни свой путь в мир электро автомобилей у нас</h1>
            <h2>Мы компания "AiC electric cars" занимаемся доставкой авто из Китая</h2>
            <a href="#about" class="btn-get-started scrollto">Узнать больше</a>
        </div>
    </section><!-- End Hero -->

    <!-- ======= Header ======= -->
    <header id="header" class="d-flex align-items-center">
        <div class="container d-flex align-items-center justify-content-between">

            <div class="logo">
                {{--                <h1 class="text-light"><img src={{asset("assets/img/logo.jpg")}} alt="{{asset("assets/img/logo.jpg")}}"></h1>--}}
                <h1 class="text-light">AiC</h1>
                <!-- Uncomment below if you prefer to use an image logo -->
                <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
            </div>

            <nav id="navbar" class="navbar">
                <ul>
                    <li><a class="nav-link scrollto active" href="#hero">Главная</a></li>
                    <li><a class="nav-link scrollto" href="#about">О нас</a></li>
                    <li><a class="nav-link scrollto" href="#services">Услуги</a></li>
                    <li><a class="nav-link scrollto " href="#portfolio">Каталог</a></li>
                    <li><a class="nav-link scrollto" href="#contact">Контакты</a></li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav><!-- .navbar -->

        </div>
    </header><!-- End Header -->

    <main id="main">

        <!-- ======= About Section ======= -->
        <section id="about" class="about">
            <div class="container">

                <div class="row">
                    <div style="background-image: url(assets/img/about.jpg)" data-aos="fade-right"
                         class="image col-xl-5 d-flex align-items-stretch justify-content-center justify-content-lg-start"></div>
                    <div class="col-xl-7 pt-4 pt-lg-0 d-flex align-items-stretch">
                        <div class="content d-flex flex-column justify-content-center" data-aos="fade-left">
                            <h3>Компания с многолетним опытом</h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit
                            </p>
                            <div class="row">
                                <div class="col-md-6 icon-box" data-aos="zoom-in" data-aos-delay="100">
                                    <i class="bx bx-receipt"></i>
                                    <h4>Corporis voluptates sit</h4>
                                    <p>Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut
                                        aliquip</p>
                                </div>
                                <div class="col-md-6 icon-box" data-aos="zoom-in" data-aos-delay="200">
                                    <i class="bx bx-cube-alt"></i>
                                    <h4>Ullamco laboris nisi</h4>
                                    <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                                        deserunt</p>
                                </div>
                                <div class="col-md-6 icon-box" data-aos="zoom-in" data-aos-delay="300">
                                    <i class="bx bx-images"></i>
                                    <h4>Labore consequatur</h4>
                                    <p>Aut suscipit aut cum nemo deleniti aut omnis. Doloribus ut maiores omnis
                                        facere</p>
                                </div>
                                <div class="col-md-6 icon-box" data-aos="zoom-in" data-aos-delay="400">
                                    <i class="bx bx-shield"></i>
                                    <h4>Beatae veritatis</h4>
                                    <p>Expedita veritatis consequuntur nihil tempore laudantium vitae denat pacta</p>
                                </div>
                            </div>
                        </div><!-- End .content-->
                    </div>
                </div>

            </div>
        </section><!-- End About Section -->

        <!-- ======= Clients Section ======= -->
        <section id="clients" class="clients">

        </section><!-- End Clients Section -->

        <!-- ======= Features Section ======= -->
        <section id="features" class="features">
            <div class="container">

                <div class="row">
                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                        <div class="card" data-aos="fade-up">
                            <img src="assets/img/features-1.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <i class="bx bx-tachometer"></i>
                                <h5 class="card-title">Наша задача</h5>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor
                                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                    ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 mt-5 mt-md-0 d-flex align-items-stretch">
                        <div class="card" data-aos="fade-up" data-aos-delay="150">
                            <img src="assets/img/features-2.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <i class="bx bx-file"></i>
                                <h5 class="card-title">Наш план</h5>
                                <p class="card-text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                    doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                    veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 mt-5 mt-lg-0 d-flex align-items-stretch">
                        <div class="card" data-aos="fade-up" data-aos-delay="300">
                            <img src="assets/img/features-3.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <i class="bx bx-show"></i>
                                <h5 class="card-title">Наше видение</h5>
                                <p class="card-text">Nemo enim ipsam voluptatem quia voluptas sit aut odit aut fugit,
                                    sed quia magni dolores eos qui ratione voluptatem sequi nesciunt Neque porro
                                    quisquam est, qui dolorem ipsum quia dolor sit amet. </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- End Features Section -->

        <!-- ======= Cta Section ======= -->
        <section id="cta" class="cta">
            <div class="container">

                <div class="text-center" data-aos="zoom-in">
                    <h3>Начни сейчас</h3>
                </div>

            </div>
        </section><!-- End Cta Section -->

        <!-- ======= Services Section ======= -->
        <section id="services" class="services">
            <div class="container">

                <div class="section-title" data-aos="fade-down">
                    <span>Услуги</span>
                    <h2>Услуги</h2>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                            <i class="bi bi-truck" style="color: #0ea5e0;"></i>
                            <h4>Доставка авто из Китая</h4>
                            <p>Качественная доставка авто с пожизненной гарантией</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="icon-box" data-aos="fade-up" data-aos-delay="200">
                            <i class="bi bi-tools" style="color: #07cc70;"></i>
                            <h4>Делаем из вашего бензинового авто в электрический</h4>
                            <p>Минимальное вложение для светлого, и главное, недорогого будущего!</p>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- End Services Section -->

        <!-- ======= Portfolio Section ======= -->
        <section id="portfolio" class="portfolio section-bg">
            <div class="container">

                <div class="section-title" data-aos="fade-down">
                    <span>Каталог</span>
                    <h2>Каталог</h2>
                    <p>У нас представлен самый актуальный ассортимент электро авто/мото/вело трансорта</p>
                </div>

                <div class="row" data-aos="fade-up" data-aos-delay="150">
                    <div class="col-lg-12 d-flex justify-content-center">
                        <ul id="portfolio-flters">
                            <li data-filter="*" class="filter-active">Все</li>
                            @foreach(categories() as $category)
                                <li data-filter=".{{$category->name}}">{{$category->name}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="300">
                        @foreach(categories() as $category)
                            @foreach($category->cars as $car)
                                <div class="col-lg-4 col-md-6 portfolio-item {{$category->name}}">
                                    <div class="portfolio-wrap">
                                        <img src={{asset($car->picture)}} class="img-fluid"
                                             alt="{{asset($car->picture)}}">
                                        <div class="portfolio-info">
                                            <h4>{{$car->name}}</h4>
                                        </div>
                                        <div class="portfolio-links">
                                            <a href="{{route('cars.show', $car)}}">Подробнее</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                </div>

            </div>
        </section><!-- End Portfolio Section -->
        <!-- ======= Team Section ======= -->
        <section id="clients" class="clients">

        </section><!-- End Clients Section -->

        <!-- ======= Contact Us Section ======= -->
        <section id="contact" class="contact">
            <div class="container">

                <div class="section-title" data-aos="fade-down">
                    <span>Свяжитесь с нами</span>
                    <h2>Свяжитесь с нами</h2>
                </div>

                <div class="row justify-content-center">
                    <div class="col-lg-4 col-md-12" data-aos="fade-up" data-aos-delay="100">
                        <div class="info-box">
                            <i class="bx bx-map"></i>
                            <h3>Наш адрес</h3>
                            <p>A108 Adam Street, New York, NY 535022</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="200">
                        <div class="info-box">
                            <i class="bx bx-envelope"></i>
                            <h3>Наша почта</h3>
                            <p>info@example.com<br>contact@example.com</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="300">
                        <div class="info-box">
                            <i class="bx bx-phone-call"></i>
                            <h3>Наши контакты</h3>
                            <p>+1 5589 55488 55<br>+1 6678 254445 41</p>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- End Contact Us Section -->

    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <footer id="footer">

        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Наши социальные сети</h4>
                        <div class="social-links mt-3">
                            <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container py-4">
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/plato-responsive-bootstrap-website-template/ -->
            </div>
        </div>
    </footer><!-- End Footer -->

@endsection
